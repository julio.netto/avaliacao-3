#!/bin/bash

if [ -n "$PS1_CUSTOM" ]; then
	export PS1="$PS1_BACKUP"
	unset PS1_CUSTOM

	echo "Prompt resetado com êxito."
else
	echo "O prompt ja está no padrão"
fi
