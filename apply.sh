#!/bin/bash

source func.sh

EXIBIR="$1"
DIVIDIR="$2"
COR="$3"

echo "Variáveis em apply.sh: EXIBIR=$EXIBIR, DIVIDIR=$DIVIDIR, COR=$COR"

if [ "$EXIBIR" = true ]; then
	exibir_apenas_usuario
fi

if [ "$DIVIDIR" = true ]; then
	dividir_linhas
fi

if [ -n "$COR" ]; then
	mudar_cor "$COR"
fi

echo "Mudanças aplicadas no prompt"


