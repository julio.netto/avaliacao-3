#!/bin/bash

VERDE="\033[0;32m"
AMARELO="\033[0;33m"
AZUL="\033[0;34m"
VERMELHO="\033[0;31m"
MAGENTA="\033[0;35m"
CIANO="\033[0;36m"
BRANCO="\033[0;37m"
PRETO="\033[0;30m"

mudar_cor() {
	export PS1="$1\u@\h:\w\$\[\033[0m\] "
}

exibir_apenas_usuario() {
	export PS1="\u@\h:\w\$ "
}

dividir_linhas() {
	export PS1="\u@\h:\w"$'\n$ '
}
