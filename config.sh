#!\bin\bash

source func.sh

EXIBIR="False"
DIVIDIR="False"

menu (){
	echo "1. Mudar cor"
	echo "2. Exibir apenas nome do usuário"
	echo "3. Dividir linha do prompt"
	echo "4. Encerrar"
}

cores (){
	echo "1. Verde"
	echo "2. Amarelo"
	echo "3. Azul"
	echo "4. Ciano"
	echo "5. Vermelho"
	echo "6. Magenta"
	echo "7. Branco"
        echo "8. Preto"
	echo "9. Cancelar"
}
while true; do
	menu
	echo
        read -p "Opção: " opcao
	echo
        case $opcao in
		1) cores
                   echo
                   read -p "Escolha uma cor: " COR
                   echo
                   case $COR in
                           1) COR="$VERDE";;
                           2) COR="$AMARELO";;
                           3) COR="$AZUL";;
                           4) COR="$CIANO";;
                           5) COR="$VERMELHO";;
                           6) COR="$MAGENTA";;
                           7) COR="$BRANCO";;
                           8) COR="$PRETO";;
                   esac;;
		2) EXIBIR="true"
			;;
		3) DIVIDIR="true"
			;;
		4) echo "Operação cancelada"
			break 2
			;;
		*) echo "Opção inválida"
	esac
done

export EXIBIR
export DIVIDIR
export COR

echo "Variáveis exportadas: EXIBIR=$EXIBIR, DIVIDIR=$DIVIDIR, COR=$COR"

bash apply.sh "$EXIBIR" "$DIVIDIR" "$COR"
